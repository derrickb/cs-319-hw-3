// Derrick Brandt
// HW 3
// MVC Pracitce

// Note Snake here is an object that consists of several objects.
//   Model: stores data
//   View: used for display 
//   Controller: decides what happens on key clicks
//
//   The run method is called FIRST.  It sets up everything by calling
//   helper functions.
//   First, it sets button values by calling assignButtonValues 
//   Next, it attaches button onclick handlers by calling attachHandlers
//   Next, it uses displayAll to return html to be displayed on browser
//

// ------------------NOTE----------------------------------------
//create a while loop that increments the x or y value and draws
// To move right Y val stays the same and x changes
// To move left Y val stays the same and x changes +-x, y.

// to move up X val stays the same and y val changes
// to move down X val stays the same and y val changes x, +-y
// ------------------END NOTE----------------------------------------

const zero = 0;


var Snake = {

Model : {
    //#TODO
    x_val : undefined,
    y_val : undefined,
    previousCoordinates : [],
}, 

View : {
    //#TODO
    startStopBtn : document.getElementById("startStopBtn"),
    leftBtn      : document.getElementById("leftBtn"),
    rightBtn     : document.getElementById("rightBtn"),
    canvas       : document.getElementById("canvasId"),

    render       : (a, b) => {
        const draw = Snake.View.canvas.getContext("2d");

        //initialize look
        draw.lineWidth = 1;
        draw.strokeStyle = 'red';

        //begin
        draw.beginPath();
        draw.moveTo(Snake.Model.x_val, Snake.Model.y_val);
        draw.lineTo(10, Snake.Model.y_val);
        draw.stroke();
        
        draw.lineTo(10, Snake.Model.y_val - 50)
        draw.stroke();

        //test to see if things stay on canvas on next draw
        // draw.moveTo(0,0);
        // draw.lineTo(Snake.Model.x_val, Snake.Model.y_val/2);
        // draw.stroke();
        
    }
},

Controller : {
    //#TODO
    handleLeftClick : () => {

    },

    handleRightClick : () => {

    },
    handleStartClick : () => {

    },

    handleStopClick : () => {

    },

    timer : () => {

    },

    increment : ()=> {
        // change x_val and y_val and call render
        // store old values in Model.previousCoordinates
    }
},

run : () => {
    //#TODO
    Snake.intializeXY();
    Snake.attachHandlers();
},

assignButtonValues : () => {

},

attachHandlers : () => {
    Snake.View.startStopBtn.addEventListener("click", ()=>{
    Snake.View.render();
    Snake.Model.previousCoordinates.push([Snake.Model.x_val, Snake.Model.y_val])
    });

    Snake.View.leftBtn.addEventListener("click", ()=>{
        //#TODO 
        // how to make the snake always turn left?
    } )

    Snake.View.rightBtn.addEventListener("click", ()=>{
        //#TODO 
        // how to make the snake always turn right?
    } )

},

intializeXY : () => {
    //start at left of screen with half height
    Snake.Model.x_val = zero;
    Snake.Model.y_val = Snake.View.canvas.height/2;
},

initializeTimer : () => {
    var timer = setInterval(() => {
        
    }, 1000)
}


} // end of Snake